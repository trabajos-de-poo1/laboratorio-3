const  convert  =  require ( 'geo-coordinates-parser') ;

let converted;
try {
  converted = convert('55° 26.7717, -105° 56.93172');

}
catch {
  /* llegamos aquí si la cadena no tiene coordenadas válidas o el formato es inconsistente entre lat y long */
}

console.log ('la coordenada para latitud ingresada en decimales es: ')
console.log (converted.decimalLatitude);
console.log ('la coordenada para longitud ingresada en decimales es: ')
console.log (converted.decimalLongitude);


